
import {useState,useEffect, useContext} from 'react'
import {Form, Button,Card,Container,Row,Col} from 'react-bootstrap'
import {Redirect,Link} from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'


export default function Login(){

	//Allow us to consume the User Context object and it's properties to use for user validation
	const {user, setUser} = useContext(UserContext)
	const [password,setPassword] = useState('')
	const [email,setEmail] = useState('')
	const [show,setShow] = useState(false)
	const [isActive, setIsActive] = useState(false)
	console.log(show)
	console.log(email)
	console.log(password)

	function Aunthenticate(e){
		
		//Prevent page redirection via form submission
		e.preventDefault();

		fetch('https://afternoon-sea-26313.herokuapp.com/users/login',{
			method:'POST',
			headers:{
				'Content-Type' : 'application/json'
			},
			body:JSON.stringify({
				email:email,
				password:password
			})
		})

		.then(res => res.json())
		.then(data =>{
			console.log(data)

			if(typeof data.access !== 'undefined'){
				localStorage.setItem('token',data.access)
				localStorage.setItem('cart','[]')
				retrieveUserDetails(data.access)

				Swal.fire({
					title:'Login Successful!',
					icon:'success',
					text:'Welcome to Zuitt'
				})
			} else {
				Swal.fire({
					title:'Authentication Failed',
					icon:'error',
					text:'Check your login details'
			})
			}
		})
		

		setEmail('');
		setPassword('');

		const retrieveUserDetails = (token) => {
			fetch('https://afternoon-sea-26313.herokuapp.com/users/details',{
				method:'GET',
				headers:{
					Authorization:`Bearer ${token}`
				}
			})
			.then(res => res.json())
			.then(data =>{
				console.log(data)

				setUser({
					id:data._id,
					isAdmin:data.isAdmin
				})
			})
		}

	}

	useEffect(() => {

    if(email !== '' && password !== ''){
        setIsActive(true);
    }else{
        setIsActive(false);
    }

	}, [email, password]);

	return(
		(user.id !==null) ?
			<Redirect to = "/"/>
			:
			<Form onSubmit = {(e) => Aunthenticate(e)}>
			<Container className="mt-5">
				<Row>
					<Col  lg ={{span:6, offset:3}}>
						<Card id="formLogin" className="mx-auto" >
						<Card.Body >
						<Card.Title ><h1 className="loginTitle" >Log-In</h1></Card.Title>
						<Card.Subtitle className="mt-3"><h3>Email:</h3></Card.Subtitle>
            			<Form.Control
					 type='email'
					 placeholder='Please enter your new email here'
					 value = {email}
					 onChange = {event => setEmail(event.target.value)}
					 required
					/>

				<Card.Subtitle><h3>Password:</h3></Card.Subtitle>
            	<Form.Control
					 type='password'
					 placeholder='Please enter your Password here'
					 value = {password}
					 onChange = {event => setPassword(event.target.value)}
					 required
					/>

				{ isActive ? 
					<Button variant = 'primary' type = 'submit' id = 'submitBtn' className='my-1'>
							Login
					</Button>

					:
					<Button variant = 'danger' type = 'submit' id = 'submitBtn' className='my-1' disabled>
						Login
					</Button>
				}

				<Card.Text>if you don't have an account,Click here to <Link as={Link} to ={`/register`} exact>Register</Link></Card.Text> 
				</Card.Body>
       		 </Card>
       		 </Col>
        </Row>
      </Container>
      		 </Form>
			
		)
}
