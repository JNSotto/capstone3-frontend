import {useState,useEffect,useContext} from 'react'
import {Card,Button,Container,Row,Col} from 'react-bootstrap'
import {Link,useHistory,Redirect} from 'react-router-dom'
import EditDataCustomer from '../components/EditDataCustomer'
import UserContext from '../UserContext'


export default function CustomerViewData(){
const {user,setUser} = useContext(UserContext)
const history = useHistory()


return((user.isAdmin === true) ?
      <Redirect to = "/"/>
      :
      <Container className="mt-5">
        <Row>
          <Col  lg ={{span:6, offset:3}}>
            <Card id="customer" className="mx-auto" style={{ width: '100%' }}>
              <Card.Body>
                <Card.Subtitle><h3>Name:</h3></Card.Subtitle>
                <h4>{user.firstName} {user.lastName}</h4>
                <Card.Subtitle><h3>Email:</h3></Card.Subtitle>
                <h4>{user.email}</h4>
                <Card.Subtitle><h3>Address:</h3></Card.Subtitle>
                <h4>{user.address}</h4>
                <Card.Subtitle><h3>Contact Number:</h3></Card.Subtitle>
                <h4>{user.mobileNo}</h4>
                <Card.Link className="btn btn-success" as={Link} to ={`/customer/${user.id}`} exact>Edit Profile</Card.Link>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
  )
}