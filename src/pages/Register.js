import {useState,useEffect,useContext} from 'react'
import {Form, Button,Card,Row,Col,Container} from 'react-bootstrap'
import {Redirect,useHistory,Link} from 'react-router-dom' //6.2.0 Navigate 
import UserContext from '../UserContext'
import Swal from 'sweetalert2'


export default function Register(){

	const {user, setUser} = useContext(UserContext)
	const history = useHistory()

	//State hooks to store the values of the input fields
	const [firstName,setFirstName] = useState('')
	const [lastName,setLastName] = useState('')
	const [email,setEmail] = useState('')
	const [address,setAddress] = useState('') 
	const [mobileNo,setMobileNo] = useState('')
	const [password1,setPassword1] = useState('')
	const [password2,setPassword2] = useState('')

	//State to determine whether register button is enabled or not
	const [isActive, setIsActive] = useState(false)

	//Check if values are successfully binded
	console.log(email)
	console.log(password1)
	console.log(password2)

	//this is for clearing the input 
	function registerUser(e){
		
		//Prevents page redirection via form submission
		e.preventDefault();

		fetch('https://afternoon-sea-26313.herokuapp.com/users/checkEmail',{
				method:'POST',
				headers:{
					'Content-Type' :'application/json'
				},
				body:JSON.stringify({
					email:email
				})
			})
			.then(res => res.json())
			.then(retrieve =>{
				if( retrieve !== true){
				fetch('https://afternoon-sea-26313.herokuapp.com/users/register',{
					method:'POST',
					headers:{
						'Content-Type' : 'application/json'
					},
					body:JSON.stringify({
						firstName:firstName,
						lastName:lastName,
						email:email,
						address:address,
						mobileNo:mobileNo,
						password:password1
					})
				})

				.then(res => res.json())
				.then(register =>{
					console.log(register)
				Swal.fire({
					title:'Registration Successful!',
					icon:'success',
					text:'Welcome to Zuitt'
				})
				history.push("/login")
				//history("/login") - for Version 6
				})
			} else {
				Swal.fire({
					title:'Email Already Exist!',
					icon:'error',
					text:'Please try to another email'
			})
			}
		})

			
		//Clear the input fields
		setFirstName('');
		setLastName('');
		setEmail('');
		setAddress('');
		setMobileNo('');
		setPassword1('');
		setPassword2('');

		//alert('Thank you for Registering!');



	}

	useEffect(() => {
		if((firstName !== '' && lastName !== '' && email !== '' && address !== '' && mobileNo !== '' && password1 !== '' && password2 !== '') && (password1 === password2) && (mobileNo.length === 11)) {

			setIsActive(true)

		} else {

			setIsActive(false)
		}
	}, [firstName,lastName,email,address,mobileNo,password1,password2])

	return((user.id !==null) ?
			<Redirect to = "/"/>
			:
			<Form onSubmit = {(e) => registerUser(e)}>
			<Container className="mt-5">
				<Row>
					<Col  lg ={{span:6, offset:3}}>
				<Card className=" form mx-auto">
          <Card.Body className="form">
          <Card.Title ><h1 className="loginTitle" >Register</h1></Card.Title>
            <Card.Subtitle><h3>First Name:</h3></Card.Subtitle>
					<Form.Control
					 type='firstName'
					 placeholder='Please enter your new First Name here'
					 value = {firstName}
					 onChange = {event => setFirstName(event.target.value)}
					 required
					/>
            <Card.Subtitle><h3>Last Name:</h3></Card.Subtitle>
            <Form.Control
					 type='lastName'
					 placeholder='Please enter your new Last Name here'
					 value = {lastName}
					 onChange = {event => setLastName(event.target.value)}
					 required
					/>
          <Card.Subtitle><h3>Email:</h3></Card.Subtitle>
            <Form.Control
					 type='email'
					 placeholder='Please enter your new email here'
					 value = {email}
					 onChange = {event => setEmail(event.target.value)}
					 required
					/>
            <Card.Subtitle><h3>Address:</h3></Card.Subtitle>
            <Form.Control
					 type='address'
					 placeholder='Please enter your New Complete Address here'
					 value = {address}
					 onChange = {event => setAddress(event.target.value)}
					 required
					/>
            <Card.Subtitle><h3>Contact Number:</h3></Card.Subtitle>
            <Form.Control
					 type='mobileNo'
					 placeholder='Please enter your New Mobile Number here'
					 value = {mobileNo}
					 onChange = {event => setMobileNo(event.target.value)}
					 required
					/>

					<Card.Subtitle><h3>New Password:</h3></Card.Subtitle>
            <Form.Control
					 type='password'
					 placeholder='Please enter your New Password here'
					 value = {password1}
					 onChange = {event => setPassword1(event.target.value)}
					 required
					/>

					<Card.Subtitle><h3>Verify Password:</h3></Card.Subtitle>
            <Form.Control
					 type='password'
					 placeholder='Please enter Verificatioin Password here'
					 value = {password2}
					 onChange = {event => setPassword2(event.target.value)}
					 required
					/>
            { isActive ? 
					<Button  type = 'submit' id = 'submitBtn' className=' btn btn-success'>
							Register
					</Button >
					:
					<Button  type = 'submit' id = 'submitBtn' className='  btn btn-danger' disabled>
						Register
					</Button >
			}
            	
            		<Card.Text>if you have an account, Click here to <Link as={Link} to ={`/login`} exact>Log-in</Link></Card.Text> 
				</Card.Body>
        </Card>
        </Col>
        </Row>
      </Container>
			</Form>

		
		)
}

