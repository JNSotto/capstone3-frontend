import {Fragment, useEffect, useState,useContext} from 'react'
import {Table,Button} from 'react-bootstrap'
import {Link,useHistory,Redirect} from 'react-router-dom'
import CartCard from '../components/CartCard'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Cart(){
const history = useHistory()
const [addCarts, setAddCarts] = useState([])
const {user, setUser} = useContext(UserContext)

const cartData = JSON.parse(localStorage.getItem('cart'))
console.log(cartData)
function displayTotal(){
	if( cartData.length!==0){
	let mapTotal = cartData.map(data=>data.subTotal)
	let totalPrice = mapTotal.reduce((previousValue, currentValue)=> previousValue + currentValue)
	return totalPrice
	}else{
		const message = "Cart is Empty!"
		return message
	}
}
let displayTotalPrice = displayTotal();
console.log(displayTotalPrice)


const checkOut = () => {
		fetch(`https://afternoon-sea-26313.herokuapp.com/users/addToCart`, {
			method: 'POST',
			headers : {
				"Content-Type" : "application/json",
				Authorization : `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
			customer:user.id,
			orderList:cartData
		})
		})
		.then(res => res.json()) 
		.then(data => {
			console.log(data)
			if(data === true){
				Swal.fire({
					title: 'Order Successful!',
					icon: 'success',
					text: 'You have successfully checked out the product'
				})

				history.push("/")
 				window.location.reload()
			} else {
				Swal.fire({
					title: 'Something went wrong!',
					icon: 'error',
					text: 'Please try again'
				})
			}
		})

		localStorage.setItem('cart','[]')

	}

useEffect(()=> {
	setAddCarts(cartData.map(cart => {
	return(
		<CartCard key = {cart.id} cartProp = {cart}/>
		)
	}))
},[])


	return((user.isAdmin ===true) ?
			<Redirect to = "/"/>
			:
		<Fragment> 
			<h1>Cart:</h1>
			<Table striped bordered hover variant="dark" size="sm">
			  <thead>
				    <tr>
				      <th>Product Name</th>
				      <th>Price</th>
				      <th>Quantity</th>
				      <th>subTotal</th>
				      <th>Delete</th>
				    </tr>
				  </thead>
				  {(cartData.length===0)?
				  	<tbody>
			 		<tr>	
					<td colSpan="5"><h1>Cart is Empty!</h1></td>
					</tr>
			 		</tbody>
				  	:
				  	<tbody>
					{addCarts}
			 		<tr>	
					<td colSpan="3"><Link  className="btn btn-danger" onClick = {()=>checkOut()} >Check-Out</Link></td>
				    <td colSpan="2">Total: ₱{displayTotalPrice}</td>
				    </tr>
			 		</tbody>
				  }
				  
			</Table>
		</Fragment>
		)
}

