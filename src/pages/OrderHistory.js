import {Fragment, useEffect, useState,useContext} from 'react'
import {Container,Row} from 'react-bootstrap'
import{Redirect} from 'react-router-dom'
import OrderHistoryCard from '../components/OrderHistoryCard'
import UserContext from '../UserContext'


export default function OrderHistory(){
const {user,setUser} = useContext(UserContext)
const [orders, setOrders] = useState([])

useEffect(()=> {
  fetch(`https://afternoon-sea-26313.herokuapp.com/users/${user.id}/myOrder`)
  .then(res => res.json())
  .then(data => {
    console.log(data)
  setOrders(data.map(order => {
    console.log(data)
  return(
    <OrderHistoryCard key = {order.id} orderProp = {order}/>
    )
    }))
  }) 
},[])

  return((user.isAdmin ===true) ?
      <Redirect to = "/products"/>
      :
    <Container>
      <h1>Order History</h1>
      <Row xs={1} md={2} lg={3} className="g-4">
      {orders}
      </Row>
    </Container>
    )
}