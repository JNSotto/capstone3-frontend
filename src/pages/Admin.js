import {Fragment} from 'react'
import{Tabs,Tab} from 'react-bootstrap'
import AdminViewProduct from '../components/AdminViewProduct'
import CreateProduct from '../components/CreateProduct'

export default function Admin(){
  return(
      <Tabs defaultActiveKey="ViewProduct" id="uncontrolled-tab-example" className="mb-3">
        <Tab eventKey="ViewProduct" title="View Product">
          <AdminViewProduct/>
        </Tab>
        <Tab eventKey="CreateProduct" title="Create New Product">
          <CreateProduct/>
        </Tab>
      </Tabs>
    )
}

