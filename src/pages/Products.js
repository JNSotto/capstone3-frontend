import {Fragment, useEffect, useState} from 'react'
import {Container,Row} from 'react-bootstrap'
import ProductCard from '../components/ProductCard'

export default function Products(){

const [products, setProducts] = useState([])

useEffect(()=> {
	fetch('https://afternoon-sea-26313.herokuapp.com/products/')
	.then(res => res.json())
	.then(data => {
		console.log(data)
	setProducts(data.map(product => {
	return(
		<ProductCard key = {product.id} productProp = {product}/>
		)
		}))
	}) 
},[])

	return(
		<Container className ="product-container">
			<h1 className="product-title">Products</h1>
			<Row xs={1} md={2} lg={3} className="g-4">
			{products}
			</Row>
		</Container>
		)
}