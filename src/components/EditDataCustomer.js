import {useState,useEffect,useContext} from 'react'
import {Form, Button, Card,Container,Row,Col} from 'react-bootstrap'
import {Redirect,useHistory,Link} from 'react-router-dom' //6.2.0 Navigate 
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function EditDataCustomer(){

	const {user, setUser} = useContext(UserContext)
	const history = useHistory()

	//State hooks to store the values of the input fields
	const [firstName,setFirstName] = useState('')
	const [lastName,setLastName] = useState('')
	const [email,setEmail] = useState('')
	const [address,setAddress] = useState('')
	const [mobileNo,setMobileNo] = useState('')
	const [password1,setPassword1] = useState('')
	const [password2,setPassword2] = useState('')

	//State to determine whether register button is enabled or not
	const [isActive, setIsActive] = useState(false)

	console.log(firstName)
	console.log(lastName)
	console.log(email)
	console.log(address)
	console.log(mobileNo)

	function updateUser(e){
		e.preventDefault();

		fetch('https://afternoon-sea-26313.herokuapp.com/users/checkEmail',{
				method:'POST',
				headers:{
					'Content-Type' :'application/json'
				},
				body:JSON.stringify({
					email:email
				})
			})
			.then(res => res.json())
			.then(retrieve =>{
				if( retrieve !== true){
				fetch(`https://afternoon-sea-26313.herokuapp.com/users/${user.id}`,{
					method:'PUT',
					headers:{
						'Content-Type' : 'application/json',
						Authorization : `Bearer ${localStorage.getItem("token")}`
					},
					body:JSON.stringify({
						firstName:firstName,
						lastName:lastName,
						email:email,
						password:password1,
						address:address,
						mobileNo:mobileNo
					})
				})

				.then(res => res.json())
				.then(update =>{
					console.log(update)
				Swal.fire({
					title:'Update Successful!',
					icon:'success',
					text:`${firstName}'s Data is Successfully Updated `
				})
				history.push("/login")
				})
			} else {
				Swal.fire({
					title:'Email Already Exist!',
					icon:'error',
					text:'Please try to another email'
			})
			}
		})

		setFirstName('');
		setLastName('');
		setEmail('');
		setAddress('');
		setMobileNo('');
		setPassword1('');
		setPassword2('');
	}

	useEffect(() => {
		if((firstName !== '' && lastName !== '' && email !== '' && address !== '' && mobileNo !== '' && password1 !== '' && password2 !== '') && (password1 === password2) && (mobileNo.length === 11)) {

			setIsActive(true)

		} else {

			setIsActive(false)
		}
	}, [firstName,lastName,email,mobileNo,password1,password2])

	return((user.isAdmin ===true) ?
			<Redirect to = "/"/>
			:
	<Form onSubmit = {(e) => updateUser(e)}>
				 <Card className="mx-auto" style={{ width: '100%' }}>
          <Card.Body>
            <Card.Subtitle><h3>First Name:</h3></Card.Subtitle>
					<Form.Control
					 type='firstName'
					 placeholder='Please enter your new First Name here'
					 value = {firstName}
					 onChange = {event => setFirstName(event.target.value)}
					 required
					/>
            <Card.Subtitle><h3>Last Name:</h3></Card.Subtitle>
            <Form.Control
					 type='lastName'
					 placeholder='Please enter your new Last Name here'
					 value = {lastName}
					 onChange = {event => setLastName(event.target.value)}
					 required
					/>
          <Card.Subtitle><h3>Email:</h3></Card.Subtitle>
            <Form.Control
					 type='email'
					 placeholder='Please enter your new email here'
					 value = {email}
					 onChange = {event => setEmail(event.target.value)}
					 required
					/>
            <Card.Subtitle><h3>Address:</h3></Card.Subtitle>
            <Form.Control
					 type='address'
					 placeholder='Please enter your New Complete Address here'
					 value = {address}
					 onChange = {event => setAddress(event.target.value)}
					 required
					/>
            <Card.Subtitle><h3>Contact Number:</h3></Card.Subtitle>
            <Form.Control
					 type='mobileNo'
					 placeholder='Please enter your New Mobile Number here'
					 value = {mobileNo}
					 onChange = {event => setMobileNo(event.target.value)}
					 required
					/>

					<Card.Subtitle><h3>New Password:</h3></Card.Subtitle>
            <Form.Control
					 type='password1'
					 placeholder='Please enter your New Password here'
					 value = {password1}
					 onChange = {event => setPassword1(event.target.value)}
					 required
					/>

					<Card.Subtitle><h3>Verify Password:</h3></Card.Subtitle>
            <Form.Control
					 type='password2'
					 placeholder='Please enter Verificatioin Password here'
					 value = {password2}
					 onChange = {event => setPassword2(event.target.value)}
					 required
					/>
            { isActive ? 
					<Button  type = 'submit' id = 'submitBtn' className='btn-customer btn btn-success'>
							Save
					</Button >
					:
					<Button  type = 'submit' id = 'submitBtn' className=' btn-customer btn btn-danger' disabled>
						Save
					</Button >
			}
            
            <Button className="btn-customer btn-danger" as={Link} to ={`/customer`} exact>Cancel</Button>
          </Card.Body>
        </Card>
       </Form>
  )
			
}