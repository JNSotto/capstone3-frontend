import {Fragment, useEffect, useState,useContext} from 'react'
import {Table,Container,Row,Col,Button} from 'react-bootstrap'
import {Redirect} from 'react-router-dom'
import AdminDashBoard from '../components/AdminDashBoard'
import UserContext from '../UserContext'


export default function AdminViewProduct(){
const {user,setUser} = useContext(UserContext)
const [products, setProducts] = useState([])

useEffect(()=> {
  fetch('https://afternoon-sea-26313.herokuapp.com/products/all')
  .then(res => res.json())
  .then(data => {
    console.log(data)
  setProducts(data.map(product => {
  return(
    <AdminDashBoard key = {product.id} adminProp = {product}/>
    )
    }))
  }) 

},[])

  return(
    (user.isAdmin === false) ?
      <Redirect to = "/"/>
      :
    <Container>
      <h1 className ="adminTitle">View Products</h1>
      <Row>
        <Col >  
          <Table striped bordered hover variant="dark" size="sm">
            <thead>
                <tr>
                  <th>Category</th>
                  <th>Product Name</th>
                  <th>Brand</th>
                  <th>Description</th>
                  <th>Price</th>
                  <th>InStock</th>
                  <th>Sold</th>
                  <th>Available</th>
                  <th>Date of Creation</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              {products}
              </tbody>
          </Table>
        </Col>
      </Row>
    </Container>
    )
}

