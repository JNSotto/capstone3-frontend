import {useState,useEffect,useContext} from 'react'
import {Form, Button,Col,Row} from 'react-bootstrap'
import {Redirect,useHistory} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'


export default function CreateProduct(){

	const {user, setUser} = useContext(UserContext)
	const history = useHistory()

	const [name,setName] = useState('')
	const [category,setCategory] = useState('')
	const [brand,setBrand] = useState('')
	const [description,setDescription] = useState('')
	const [price,setPrice] = useState('')
	const [inStock,setInStock] = useState('')
	const [isActive, setIsActive] = useState(false)

	function addProduct(e){
		e.preventDefault();

		fetch('https://afternoon-sea-26313.herokuapp.com/products/createProduct',{
				method:'POST',
				headers:{
					'Content-Type' :'application/json',
					Authorization : `Bearer ${localStorage.getItem("token")}`
				},
				body:JSON.stringify({
					name:name,
					category:category,
					brand:brand,
					description:description,
					price:price,
					inStock:inStock
				})
			})
			.then(res => res.json())
			.then(retrieve =>{
			if( retrieve === true){
				Swal.fire({
					title:'Added Product Successfully!',
					icon:'success',
					text:'Product is Successfully Added to the Database'
				})
				history.push("/admin")
				window.location.reload()
			} else {
				Swal.fire({
					title:'Input Is Invalid!',
					icon:'error',
					text:'Please try to Input the following forms'
			})
			}
			console.log(retrieve)
		})

		setName('');
		setCategory('');
		setBrand('');
		setDescription('');
		setPrice('');
		setInStock('');

	}

	useEffect(() => {
		if(name !== '' && category !== '' && brand !== '' && description !== '' && price !== '' && inStock !== '') {

			setIsActive(true)

		} else {

			setIsActive(false)
		}
	}, [name,category,brand,description,price,inStock])

	return((user.isActive === false) ?
			<Redirect to = "/"/>
			:
			<Form onSubmit = {(e) => addProduct(e)}>
				<Form.Group as={Col} className='my-1'>
					<Form.Label>Product Name:</Form.Label>
					<Form.Control
					 type='name'
					 placeholder='Please Enter the Product Name here'
					 value = {name}
					 onChange = {event => setName(event.target.value)}
					 required
					/>
				</Form.Group>				

			<Form.Group as={Col} className='my-1'>
					<Form.Label>Category:</Form.Label>
					<Form.Control
					 type='category'
					 placeholder='Please Enter Category of the Product here'
					 value = {category}
					 onChange = {event => setCategory(event.target.value)}
					 required
					/>
				</Form.Group>

			<Form.Group as={Col} className='my-1'>
					<Form.Label>Brand:</Form.Label>
					<Form.Control
					 type='brand'
					 placeholder='Please Enter Brand of the Product here'
					 value = {brand}
					 onChange = {event => setBrand(event.target.value)}
					 required
					/>
				</Form.Group>

			<Form.Group className='my-1'>
					<Form.Label>Description:</Form.Label>
					<Form.Control
					 type='Description'
					 placeholder='Please Enter Description of the Product here'
					 value = {description}
					 onChange = {event => setDescription(event.target.value)}
					 required
					/>
				</Form.Group>							

			<Form.Group as={Col} className='my-1'>
					<Form.Label>Price:</Form.Label>
					<Form.Control
					 type='Number'
					 placeholder='Please Input Price of the Product here'
					 value = {price}
					 onChange = {event => setPrice(event.target.value)}
					 required
					/>
				</Form.Group>

				<Form.Group as={Col} className='my-1'>
					<Form.Label>InStock:</Form.Label>
					<Form.Control
						type = 'Number'
						placeholder = 'Please Input the Number of Stocks of the Product here'
						value = {inStock}
						onChange = {event => setInStock(event.target.value)}
						required
					 />
				</Form.Group>

				{ isActive ? 
					<Button variant = 'primary' type = 'submit' id = 'submitBtn' className='my-1'>
							Add Product
					</Button>

					:
					<Button variant = 'danger' type = 'submit' id = 'submitBtn' className='my-1' disabled>
						Add Product
					</Button>
				}
			</Form>

		
		)
}
