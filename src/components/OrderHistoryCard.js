import {useState,useEffect} from 'react'
import {Card,Col} from 'react-bootstrap'
import {Link} from 'react-router-dom'

export default function OrderHistory ({orderProp}){
console.log(orderProp)
const {orderList,purchasedOn,status,totalAmount,_id} = orderProp
const [name, setName] = useState('')
const [price, setPrice] = useState(0)
const [product, setProduct] = useState('')
const [quantity, setQuantity] = useState(0)
const [subTotal, setSubTotal] = useState(0)
console.log(orderList)
useEffect(()=> {
  if(orderList!==null || orderList !== undefined){
  orderList.map(list => {
      setName(list.product.name)
      setPrice(list.product.price)
      setQuantity(list.quantity)
      setSubTotal(list.subTotal)
  })
  }else{
      setName('')
      setPrice(0)
      setQuantity(0)
      setSubTotal(0)
    
  }
},[orderList])
console.log(name)
  return(
      <Col xs = {12} md = {5} >
        <Card id="cardProductParent">
          <Card.Body>
            <Card.Title>
              <h5>Order:{_id}</h5>
            </Card.Title>
            <Card.Subtitle>
              Purchased On:
            </Card.Subtitle>
            <Card.Text>
              {purchasedOn}
            </Card.Text>
            <Card.Text>
              <h6>Items:</h6>
            <Card.Subtitle>{name}</Card.Subtitle>
              <ul>
            <li>Price: ₱{price}</li>
            <li>Quantity:{quantity}</li>
            <li>subTotal: ₱{subTotal}</li>
          </ul>    
        </Card.Text>
            <Card.Text>
              Total: ₱{totalAmount} 
            </Card.Text>
          </Card.Body>
             </Card>
          </Col>
  )
}