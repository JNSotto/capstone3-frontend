import {useState,useEffect,useContext} from 'react'
import {Card,Table,Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import UserContext from '../UserContext'
import Cart from '../pages/Cart'


export default function CartCard ({cartProp}){

	const {product,quantity,subTotal} = cartProp

	const {user,setUser} = useContext(UserContext)
	const [name, setName] = useState('')
	const [inStock, setInStock] = useState(0)
	const [price, setPrice] = useState(0)
	
	const [updateSubTotal, setUpdateSubTotal] = useState(subTotal)
	const [updatedQuantity, setUpdatedQuantity] = useState(quantity)
	const cartData = JSON.parse(localStorage.getItem('cart'))
console.log(user)
	
	useEffect(() =>{
		fetch(`https://afternoon-sea-26313.herokuapp.com/products/${product}`)
		.then(res => res.json())
		.then(data=>{

			setName(data.name)
			setPrice(data.price)
			setInStock(data.inStock)
		})
	},[product])




function addQuantity(){

	let increment = Math.min(updatedQuantity + 1,inStock)
	let total = increment*price
	setUpdatedQuantity(increment);
	setUpdateSubTotal(total);

const updateDataCart = {
			product:product,
			quantity:updatedQuantity+1,
			subTotal:total
		}
	const updateData = cartData.forEach((update,index) =>{
	if(product === update.product){
		cartData.splice(index,1,updateDataCart)	
	}
});
		
localStorage.setItem('cart',JSON.stringify(cartData))
}



function minusQuantity() {
	
	let decrement = Math.max(updatedQuantity - 1,0)
	let total = decrement*price
	setUpdatedQuantity(decrement); 
	setUpdateSubTotal(total);

const updateDataCart = {
			product:product,
			quantity:updatedQuantity-1,
			subTotal:total
		}	
const updateData = cartData.forEach((update,index) =>{
	if(product === update.product){
		cartData.splice(index,1,updateDataCart)	
	}
});
	
localStorage.setItem('cart',JSON.stringify(cartData))
}

function removeProduct(e){

const removeCart = cartData.forEach((remove,index) =>{
	if(product === remove.product ){
		cartData.splice(index,1)	
			}
		})
			localStorage.setItem('cart',JSON.stringify(cartData))
			window.location.reload()
			
}

	return(
				
				  
				    <tr>
				      <td>{name}</td>
				      <td>Php {price}</td>
				      <td><Link className="btn btn-danger" onClick = {minusQuantity}>-</Link>	
				      <input type="number" value={updatedQuantity}/>
				      <Link className="btn btn-primary" onClick = {addQuantity}>+</Link>		 				
					</td>
				      <td>₱{updateSubTotal}</td>
				      <td><Link className="btn btn-danger" onClick = {removeProduct}>REMOVE</Link></td>
				    </tr>
				      	  	
	)
}
